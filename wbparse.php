<?php
function wb_parse($text) {
	$wb = new WB_Parse();
	$wb->tokenize($text);
	$tage = $wb->parse();
	return $tage;
}
function wb_clean($text) {
	/*$text = strtr($text, array(
		' – ' => '-',
		' – ' => '-'
	));*/
	return $text;
}

class WB_Parse {
	const TYPE_EMPTY = 1;
	const TYPE_IGNORE = 2;
	const TYPE_DATUM = 3;
	const TYPE_TERMIN = 4;
	const TYPE_DESCRIPTION = 5;
	
	static $orte = array(
		'Fl' => 'St. Goar Flieden',
		'Dö' => 'Heilige Familie Döngesmühle',
		'Sw' => 'Herz-Jesu Schweben',
		'Hh' => 'DGH Höf & Haid',
		'Sh' => 'St. Katharina',
		'St' => 'DGH Struth',
		'Str' => 'DGH Struth',
		'Str.' => 'DGH Struth'
	);

	static $wochentage = array('Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag');
	static $spezial_tage = array('Palmsonntag', 'Gründonnerstag', 'Karfreitag', 'Karsamstag', 'Ostersonntag', 'Ostermontag', 'Pfingstsonntag', 'Pfingstmontag');
	static $monate = array('Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember');
	static $monateKurz = array('Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez');
	
	protected $tokens	= array();
	protected $tage		= array();
	
	function tokenize($input) {
		$datum_pattern = sprintf("/^(%s),\s*(?:den\s*)?(\d{1,2})\.\s*(%s)\.?\s*(\d{4})\s*(?:–|-)?\s*(.*)$/", implode('|', array_merge(WB_Parse::$wochentage, WB_Parse::$spezial_tage)), implode('|', array_merge(WB_Parse::$monate, WB_Parse::$monateKurz)));
		$termin_pattern = sprintf("/^(%s|[a-z.]{1,4}\.?\s)?\s*(\d{1,2})(?::|\.)(\d{2})\s*(?:h|Uhr)?\s+(.+)/i", implode('|', array_keys(WB_Parse::$orte)));
		//var_dump($termin_pattern);
		/*
			Manchmal werden drei Ausrufezeichen verwendet um auf besondere Uhrzeiten oder Orte hinzuweisen.
			Beim Parsen stört das nur, daher raus damit.
		*/
		$input = preg_replace('/!{2,}/', '', $input);
		
		/*
			Einheitliche Zeilenumbrüche
		*/
		$input = str_replace(array("\r\n", "\r"), "\n", $input);

		$input = preg_replace('/-{3,}/', '', $input);
		
		$zeilen = explode("\n", $input);		
		$this->tokens = array();
		foreach($zeilen as $lineno => $zeile) {
			$badLine = false;
			
			$zeile = trim($zeile);
			$typ = null;
			
			$token = array(
				0 => null,
				1 => $zeile,
				2 => array()
			);
			/*
				Wenn die Zeile nur aus Leerzeichen besteht, ist der aktueller Eintrag zu Ende.
			*/
			if(empty($zeile)) {
				$token[0] = WB_Parse::TYPE_EMPTY;
			}
			/*
				Ein Tag wird immer in der Form "Sonntag, 09. Januar 2011 – Taufe des Herrn, Fest" angegeben,
				das lässt sich recht leicht in einen Timestamp umwandeln.
			*/
			elseif(preg_match($datum_pattern, $zeile, &$match)) {
				$token[0] = WB_Parse::TYPE_DATUM;
				$token[2] = $match;
			}
			/*
				Gottedienste stehen in der Form "Fl	10:00 h Festhochamt f. d. Pfarrgemeinde", wobei die ersten beiden Buchstaben für den Ort stehen (Fl = Flieden).
				Wenn kein Ort angegeben ist, gilt die vorhergehende Ortsangabe.
			*/
			elseif(preg_match($termin_pattern, $zeile, &$match)) {
				$token[0] = WB_Parse::TYPE_TERMIN;
				$token[2] = $match;
			}
			/*
				In allen anderen Fällen enthält die Zeile lediglich Textinhalt
			*/
			else{
				$token[0] = WB_Parse::TYPE_DESCRIPTION;
				$token[2] = $match;
			}
			$this->tokens[] = $token;
		}
		return $this->tokens;
	}
	
	function parse() {
		$termin = null;
		$ort = null;
		$datum = null;
		
		$aktuellerKontext = false;
		
		$this->tage = array();
		
		foreach($this->tokens as $lineno => $token) {
			if($token[0] == WB_Parse::TYPE_DATUM){
				/*
					chr(128) und chr(147) sind irgendwelche komischen sonderzeichen, die dort immer rumhängen...
				*/
				$name = $this->clean(strtr($token[2][5], array(chr(128) => '', chr(147) => '')), 'name'); 
				
				if($termin != null){
					$datum['nodes'][] = $termin;
				}
				if($datum !== null){
					$this->tage[] = $datum;
				}
				
				$datum = array(
					'type'		=> WB_Parse::TYPE_DATUM,
					'lineno'	=> $lineno,
					'name'		=> $name,
					'timestamp' => mktime(null, null, null, $this->getMonthNumber($token[2][3]), $token[2][2], $token[2][4]),
					'nodes'		=> array()
				);
				
				$aktuellerKontext = WB_Parse::TYPE_DATUM;
			}elseif($datum !== null){
				switch($token[0]) {
					case WB_Parse::TYPE_TERMIN:
						/*
							Gottedienste stehen in der Form "Fl	10:00 h Festhochamt f. d. Pfarrgemeinde", wobei die ersten beiden Buchstaben für den Ort stehen (Fl = Flieden).
							Wenn kein Ort angegeben ist, gilt die vorhergehende Ortsangabe.
						*/
						if($token[2][1]){
							$k = ucfirst(strtolower($token[2][1]));
							if(isset(self::$orte[$k])) {
								$ort = self::$orte[$k];
							}else{
								$ort = $this->clean($k, 'ort');
							}
						}
						$_ort = $ort; // lokale kopie für diesen Durchlauf anlegen, damit im Folgenden der globale Ort nicht überschrieben wird
						
						$titel = $token[2][4];
						/*
							Außergewöhnliche Orte werden im Titel angegeben: "Hl. Messe in St. Michael Neuhof". Das lässt sich in der Regel ganz gut erkennen.
						*/
						if(preg_match("/(.+) in (.+)/", $titel, &$match)){
							$titel = $match[1];
							$_ort = $match[2];
							$_ort = $this->clean($_ort, 'ort');
						}
						$titel = $this->clean($titel, 'titel');
						
						if($termin != null){
							$datum['nodes'][] = $termin;
						}
						$termin = array(
							'type'			=> WB_Parse::TYPE_TERMIN,
							'lineno'		=> $lineno,
							'title'			=> $titel,
							//'day'			=> &$datum,
							'timestamp' 	=> strtotime(date('Y-m-d', $datum['timestamp']) . sprintf(" %d:%d", $token[2][2], $token[2][3])),
							'ort'			=> $_ort,
							'description'	=> '');
						
						$aktuellerKontext = WB_Parse::TYPE_TERMIN;
					break;
					case WB_Parse::TYPE_EMPTY:
						// eine leere Zeile bedeutet immer, dass ein Termin fertig ist
						
						$aktuellerKontext = false;
					break;
					case WB_Parse::TYPE_DESCRIPTION:
						/*
							Alles andere wird an den letzten Termin als Beschreibung angehängt. Überflüssige Textpassagen, die nicht zu einem konkreten Termin gehören
							können nicht erkannt werden und müssen manuell entfernt werden. Ist aber einfacher, als wegen falscher Erkennung fehlendes ergänzen zu müssen.
							
							Wenn gerade kein Termin offen ist, gehört der Text vermutlich zum Tag.
						*/
						switch($aktuellerKontext) {
							case WB_Parse::TYPE_TERMIN:
								$termin['description'] .= $this->clean($token[1], 'text') . "\n";
							break;
							case WB_Parse::TYPE_DATUM:
								$datum['name'] .= $this->clean($token[1], 'text');
							break;
							case false:
								/*
									Die letzte Zeile war leer, d.h. wir haben den letzten Kontext verlassen.
									Somit entfällt jeglicher Inhalt, da er nicht zugeordnet werden kann.
								*/
							break;
						}
					break;
					case null:
					break;
				}
			}else{
				/*
					Solange noch kein Tag eingelesen wurde, können sämtliche Zeilen ignoriert werden.
					Am Anfang stehen immer Kontaktdaten und ähnliches.
				*/
				continue;
			}
		}
		if($datum !== null){
			if($termin != null){
				$datum['nodes'][] = $termin;
			}
			$this->tage[] = $datum;
		}
		return $this->tage;
	}
	function clean($s, $typ = null) {
		$s = trim($s);
		$s = preg_replace("/\s{2,}/", ' ', $s);
		
		switch($typ) {
			case 'titel':
			case 'name':
				$s = strtr($s, array(
					'Hl. Messe' => 'Heilige Messe',
					'hl. Messe' => 'Heilige Messe',
					'hl Messe' => 'Heilige Messe',
					'f. d.' => 'für die',
					'Messdienerstd.' => 'Messdienerstunde',
					'f. geistl. Berufe' => 'für geistliche Berufe',
					'Keine Schülermesse' => ''
				));
				$s = strtr($s, array(
					'f.' => 'für'
				));
			break;
			case 'ort':
				$s = strtr($s, array(
					'der Pfarrkirche' => 'St. Goar Flieden',
					'der Pfarrkirche,' => 'St. Goar Flieden',
					'Pfarrkirche' => 'St. Goar Flieden',
					'Pfarrkirche Flieden' => 'St. Goar Flieden',
				));
			break;
			case 'text':
				/*$s = strtr($s, array(
					'f. d.' => 'für die',
					'f.' => 'für',
					'u.' => 'und',
					'Jtg.' => 'Jahrtag',
					'anschl.' => 'anschließend',
					'Hl. Messe' => 'Heilige Messe',
					'lb.' => 'lieben',
					'verst. Tochter' => 'verstorbene Tochter'
				));*/
			break;
			default:
			break;
			case null:
			break;
		}
		//$s = utf8_decode($s);
		//$s = utf8_encode($s);
		return $s;
	}
	protected function getMonthNumber($string) {	
		$i = array_search(mb_substr($string, 0, 3), self::$monateKurz);
		//var_dump($i);
		if($i === false)
			//var_dump(array_search(mb_substr($string, 0, 3), self::$monateKurz));
			throw new Exception("Unknown month $string");

		return $i + 1;
	}
	function printTokenStream() {
		$len = 6;
		$lines = ceil(log(count($this->tokens), 10));
		print $lines . "\n";
		
		foreach($this->tokens as $lineno => $token) {
			print str_pad($lineno, $lines) . " ";
			switch($token[0]){
				case WB_Parse::TYPE_DATUM:
					print str_pad('datum', $len, ' ', STR_PAD_LEFT) . ' ';
				break;
				case WB_Parse::TYPE_TERMIN:
					print str_pad('termin', $len, ' ', STR_PAD_LEFT) . ' ';
				break;
				case WB_Parse::TYPE_IGNORE:
				case WB_Parse::TYPE_EMPTY:
					print str_pad('#', $len, ' ', STR_PAD_LEFT) . ' ';
				break;
				case WB_Parse::TYPE_DESCRIPTION:
					print str_pad('>', $len, ' ', STR_PAD_LEFT) . '   ';
				break;
				case null:
					print str_pad('!!!', $len, ' ', STR_PAD_LEFT) . ' ';
				break;
				default:
					print "\n";
					var_dump($token[2]);
					print "\n";
				break;
			}
			print $token[1];
			print "\n";
		}
	}
	function getTokenTableData($tokens, $tage, $options = array()) {
		$options += array(
			'editable' => true
		);
		$type_classes = array(
			WB_Parse::TYPE_DATUM	=> 'datum',
			WB_Parse::TYPE_TERMIN	=> 'termin',
			WB_Parse::TYPE_IGNORE	=> 'ignore',
			WB_Parse::TYPE_EMPTY	=> 'empty',
			WB_Parse::TYPE_DESCRIPTION => 'description'
		);

		$nodes = array();//0 => array('type' => WB_Parse::TYPE_EMPTY));
		foreach($tage as $datum) {
			$nodes[$datum['lineno']] = $datum;
			foreach($datum['nodes'] as $termin){
				$nodes[$termin['lineno']] = $termin;
			}
		}
		
		$node = current($nodes);
		$i = 0;
		
		$rows = array();
		foreach($tokens as $lineno => $token) {
			$row = array();
			$classes = array();
			if(isset($type_classes[$token[0]])){
				$classes[] = $type_classes[$token[0]];
			}
			$empty = $token[0] === WB_Parse::TYPE_EMPTY;
			
			$row = array(
				'classes'	=> implode(' ', $classes),
				'lineno' 	=> $empty ? '' : $lineno,
				'line'		=> $token[1],
				'rowspan'	=> 0
			);
			
			if(!$empty){
				switch($token[0]){
					case WB_Parse::TYPE_DATUM:
						$row['type'] = '==';
					break;
					case WB_Parse::TYPE_TERMIN:
						$row['type'] = '+';
					break;
					case WB_Parse::TYPE_IGNORE:
					case WB_Parse::TYPE_EMPTY:
						$row['type'] = '#';
					break;
					case WB_Parse::TYPE_DESCRIPTION:
						$row['type'] = '>';
					break;
					case null:
						$row['type'] = '!!!';
					break;
				}
			}
			
			$node = current($nodes);
			if(isset($nodes[$lineno])){
				if(next($nodes)){
					$next = key($nodes);
				}else{
					$next = count($tokens);
				}
				$row['rowspan'] = $next - $node['lineno'];
				$row['node'] 	= $node;//$options['editable'] ? $node->printEdit('wbparse-' . $i++) : $node->printData();
			}
			
			$rows[] = $row;
		}
		return $rows;
	}
	static $nodeData = array(
		WB_Parse::TYPE_DATUM 	=> array(
			'edit' => array()
		),
		WB_Parse::TYPE_TERMIN	=> array(
			'edit' => array('timestamp', 'title', 'ort', 'description')
		)
	);
	static function getNodeData($node, $purpose){
		$data = array();
		
		foreach(WB_Parse::$nodeData[$node['type']][$purpose] as $k){
			$data[$k] = $node[$k];
		}
		
		if(isset($data['timestamp'])){
			$data['timestamp'] = date('Y-m-d H:i', $data['timestamp']);
		}
		return $data;
	}
	
	public static function createNodeDisplay($node) {
		$data = self::getNodeData($node, 'display');
		$return = '';
		foreach($data as $key => $value) {
			if($value === null || $value === '')
				continue;
			
			$return .= sprintf('<div><strong>%s</strong>: <span class="%s">%s</span></div>', $key, $key, $value);
		}
		return $return;
	}
	public static function createNodeEditForm($node, $fid) {
		$data = self::getNodeData($node, 'edit');
		
		$return = '';
		foreach($data as $key => $value) {
			if($key !== 'description'){
				$input = sprintf('<input name="%s" type="text" class="%s" value="%s" />', $fid . '-' . $key, $key, $value);
			}else{
				$rows = substr_count($value, "\n") + 1;
				$input = sprintf('<textarea name="%s" rows="%d">%s</textarea>', $fid . '-' . $key, $rows, $value);
			}
			$return .= sprintf('<div class="form-item"><label>%s</label>: <div>%s </div></div>', $key, $input);
		}
		return $return;
	}
}