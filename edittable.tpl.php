<table id="wbparse-edit">
	<col style="width: 3em;" />
	<col style="width: 2em;" />
	<col style="width: 40em;" />
	<col style="width: auto" />
	<tbody>
	<?php foreach($rows as $row) : $fid = 'wbparse-' . $row['lineno']; ?>
		<tr class="<?php print $row['classes']; ?>">
			<td class="lineno"><?php print $row['lineno']; ?></td>
			<td class="type"><?php print $row['type']; ?></td>
			<td class="line"><?php print $row['line']; ?></td>
			<?php if($row['rowspan']) : ?>
			<td rowspan="<?php print $row['rowspan']; ?>" class="node">
				<?php
				if($row['node']['type'] == WB_Parse::TYPE_TERMIN){
					$suggest = $row['node']['duplicates']['suggest'];
					if($suggest != 'none'){						
						$data = WB_Parse::getNodeData($row['node'], 'edit');
						?>
						<div class="form-item form-item-title"><label><?php print t('Title'); ?></label> <div>
							<input name="<?php print $fid; ?>-title" type="text" class="title" value="<?php print $data['title']; ?>" />
						</div></div>
						<div class="form-item form-item-time <?php if(strtotime($data['timestamp']) < time() ){ print ' error'; } ?>"><label><?php print t('Time'); ?></label> <div>
							<input name="<?php print $fid; ?>-time" type="text" class="timestamp" value="<?php print $data['timestamp']; ?>" />
							
						</div></div>
						<div class="form-item form-item-location"><label><?php print t('Location'); ?></label> <div>
							<input name="<?php print $fid; ?>-location" type="text" class="location" value="<?php print $data['ort']; ?>" />
						</div></div>
						<div class="form-item form-item-description"><label><?php print t('Description'); ?></label> <div>
							<textarea name="<?php print $fid; ?>-description" rows="<?php print substr_count($data['description'], "\n") + 1; ?>"><?php print $data['description']; ?></textarea>
						</div></div>
						<?php
					}
					if($row['node']['duplicates']['likeliness']){
						?>
						
						<input type="hidden" name="<?php print $fid;?>-duplicate-nid" value="<?php print $row['node']['duplicates']['nid']; ?>" />
						<?php if($suggest == 'none') : ?>
						<input type="hidden" name="<?php print $fid;?>-action" value="none" />
						<?php endif; ?>
						<?php if($suggest !== 'none'): ?>
						<select name="<?php print $fid; ?>-action" <?php 
								if($suggest == 'none')	{ print 'disabled="disabled"'; } ?>>
							<option value="update" <?php
								if($suggest == 'update')	{ print 'selected="selected" style="font-weight: bold;"'; } ?>>Bestehenden Eintrag überschreiben</option>
							<option value="keep" <?php
								if($suggest == 'none') { print 'selected="selected" style="font-weight: bold;" '; } ?>>Bestehenden Eintrag behalten</option>
							<option value="new" <?php 
								if($suggest == 'new')	{ print 'selected="selected style="font-weight: bold;" " '; }?>>Neuen Eintrag erstellen und beide behalten</option>
						</select>
						<?php endif; ?>
						
						<?php
						//krsort($row['node']['duplicates']['nodes']);
						foreach($row['node']['duplicates']['nodes'] as $dup){
							if($dup['likeliness'] < $row['node']['duplicates']['likeliness'] - 2){
								// we dont need unlikely items beeing displayed when there are more likely ones
								continue;
							}
							
							$node = $dup['node'];
							$l = array(
								7 => 'identisch',
								6 => 'Beschreibung geändert',
								5 => 'Titel geändert',
								4 => 'Titel und Beschreibung geändert',
								3 => 'Ort geändert',
								2 => 'Titel und Uhrzeit identisch',
								1 => 'Uhrzeit und Beschreibung gleich',
								0 => 'lediglich gleiche Uhrzeit'
							);
							$colors = array(
								'new' => '#DFE',
								'update' => '#FEC',
								'none' => '#EEE',
							);
							?>
							<div style="background-color: <?php print $colors[$dup['suggest']]; ?>;">
							<div style="float: right;">
							<?php
							print $dup['likeliness'] . " - " . $l[$dup['likeliness']];
							?>
							</div>
							<div><a href="<?php print url('node/' . $node->nid); ?>"><strong><?php print $node->title; ?></strong></a> 
							</div>
							<?php if($dup['suggest'] == 'update'): ?>
							<div><small><?php print date('Y-m-d H:i', $node->created); ?> by <a href="<?php print url('user/' . $node->uid); ?>"><?php print $node->name; ?></a></small></div>
							<div><strong>Ort:</strong> <?php print $node->field_location[0]['value']; ?></div>
							<div><strong>Beschreibung:</strong> <?php print nl2br($node->body); ?></div>
							<?php endif; ?>
							<?
						}
					}else{
						?>
						<input type="hidden" name="<?php print $fid;?>-action" value="new" />
						<?php
					}
				}
				?>
			</td>
			<?php endif; ?>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>