<?php
abstract class WB_Parse_Node {
	protected $nodes = array();
	protected $attributes = array('name' => null,
		'beschreibung' => '');
	protected $lineno;
	
	protected $_xmlElement = 'node';
	
	function __construct($lineno, $name) {
		$this->attributes['name'] = $name;
		$this->lineno = $lineno;
	}
	function __get($name) {
		return $this->attributes[$name];
	}
	function __set($name, $value) {
		$this->attributes[$name] = $value;
	}
	function __isset($name) {
		return isset($this->attributes[$name]);
	}
	public function addName($name) {		
		$this->name .= $name;
	}
	public function addBeschreibung($beschreibung) {
		if(strlen($this->beschreibung))
			$this->beschreibung .= "\n";
		
		$this->beschreibung .= $beschreibung;
	}
	public function getBeschreibung() {
		return rtrim($this->beschreibung);
	}
	public function getLine() {
		return $this->lineno;
	}
	public function getNodes() {
		return $this->nodes;
	}
	public function __toString() {
		return $this->printNode();
	}
	protected function _getData($purpose = null){
		return $this->attributes;
	}
	public function printData() {
		$data = $this->_getData();
		$return = '';
		foreach($data as $key => $value) {
			if($value === null || $value === '')
				continue;
			
			$return .= sprintf('<div><strong>%s</strong>: <span class="%s">%s</span></div>', $key, $key, $value);
		}
		return $return;
	}
	public function printEdit($id) {
		$data = $this->_getData('edit');
		$return = '';
		foreach($data as $key => $value) {
			if($key !== 'beschreibung'){
				$input = sprintf('<input name="%s" type="text" class="%s" value="%s" />', $id . '-' . $key, $key, $value);
			}else{
				$rows = substr_count($value, "\n") + 1;
				$input = sprintf('<textarea name="%s" rows="%d">%s</textarea>', $id . '-' . $key, $rows, $value);
			}
			$return .= sprintf('<div class="form-item"><label>%s</label>: <div>%s </div></div>', $key, $input);
		}
		return $return;
	}
	
	public static function serialize(array $nodes, $asDom = false) {
		$dom = new DOMDocument('1.0', 'UTF-8');
        $dom->formatOutput = true;
        $dom->appendChild($xml = $dom->createElement('wbparse'));

        foreach ($nodes as $n) {
            if (null === $n) {
                continue;
            }

            $child = $n->toXml(true)->documentElement->firstChild;
            $child = $dom->importNode($child, true);
            //$child->setAttribute('name', $name);

            $xml->appendChild($child);
        }

        return $asDom ? $dom : $dom->saveXml();
	}
	protected function addXmlData($node, $dom){
		$this->_addXmlAttributes($node, $dom, $this->attributes);
	}
	protected function _addXmlAttributes($node, $dom, $attributes){
        foreach ($attributes as $name => $value) {
			if($value === null || trim($value) === ''){
				continue;
			}
            $node->appendChild($attribute = $dom->createElement($name));
            $attribute->appendChild($dom->createTextNode($value));
        }
	}
	public function toXml($asDom = false) {
		$dom = new DOMDocument('1.0', 'UTF-8');
        $dom->formatOutput = true;
        $dom->appendChild($xml = $dom->createElement('wbparse'));

        $xml->appendChild($node = $dom->createElement($this->_xmlElement));
        //$node->setAttribute('class', get_class($this));
		$this->addXmlData($node, $dom);


        foreach ($this->nodes as $n) {
            if (null === $n) {
                continue;
            }

            $child = $n->toXml(true)->documentElement->firstChild;
            $child = $dom->importNode($child, true);
            //$child->setAttribute('name', $name);

            $node->appendChild($child);
        }

        return $asDom ? $dom : $dom->saveXml();
	}
	public static function fromXml($string){
		if($string instanceof DOMDocument) {
			$dom = $string;
		}else{
			$dom = new DOMDocument('1.0', 'UTF-8');
			$dom->loadXML($string);
		}
		$tage = array();
		foreach($dom->documentElement->childNodes as $i => $node) {
			if($node->nodeType != XML_ELEMENT_NODE || $node->tagName != 'tag'){
				continue;
			}
			
			$attributes = array();
			$termin_nodes = array();
			foreach($node->childNodes as $n){
				if($n->nodeType == XML_ELEMENT_NODE){
					if($n->tagName == 'termin'){
						$termin_nodes[] = $n;
					}else{
						$attributes[$n->tagName] = $n->hasChildNodes() ? $n->firstChild->wholeText : null;
					}
				}
			}
			$datum = new WB_Parse_DatumNode($i, $attributes['name'], $attributes['timestamp']);
			$tage[] = $datum;
			foreach($attributes as $k => $v){
				$datum->$k = $v;
			}
			
			foreach($termin_nodes as $n){
				$attributes = array();
				foreach($n->childNodes as $n2){
					if($n2->nodeType == XML_ELEMENT_NODE){
						$attributes[$n2->tagName] = $n2->hasChildNodes() ? $n2->firstChild->wholeText : null;
					}
				}
				$termin = new WB_Parse_TerminNode($i, $attributes['name'], $datum, $attributes['timestamp'], $attributes['ort']);
				foreach($attributes as $k => $v){
					$termin->$k = $v;
				}
			}
		}
		return $tage;
	}
}
class WB_Parse_EmptyNode extends WB_Parse_Node {
	function __construct($lineno = 0, $name = 'empty') {
		parent::__construct($lineno, $name);
	}
	protected function _getData($purpose = null){
		return array();
	}
}