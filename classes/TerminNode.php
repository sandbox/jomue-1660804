<?php
class WB_Parse_TerminNode extends WB_Parse_Node {	
	protected $_xmlElement = 'termin';
	public function __construct($lineno, $name, $datum, $hours, $minutes, $ort) {
		parent::__construct($lineno, $name);
		
		$this->attributes['timestamp'] = strtotime(date('Y-m-d', $datum->timestamp) . sprintf(" %d:%d", $hours, $minutes));
		$this->attributes['ort']  = $ort;
		
		$datum->nodes[] = $this;
	}
	
	public function getZeit() {
		return date('H:i', $this->timestamp);
	}
	
	public function printNode() {
		$beschreibung = count($this->getBeschreibung()) ? "\n\t" . $this->getBeschreibung() : '';
		return sprintf("%s %s: %s%s", date('Y-m-d H:i', $this->timestamp), $this->ort, $this->name, $beschreibung);
	}
	protected function _getData($purpose = null){
		$data = $this->attributes;
		if($purpose == 'edit'){
			$data['timestamp'] = date('Y-m-d H:i', $data['timestamp']);
		}else{
			$data['zeit'] = date('l, d. m. Y H:i', $data['timestamp']);
			unset($data['timestamp']);
		}
		return $data;
	}
	public function printData($data = false) {
		$data = $this->attributes;
		return parent::printData($data);
	}
	protected function addXmlData($node, $dom){
		$attributes = $this->attributes;
		unset($attributes['timestamp']);
		$this->_addXmlAttributes($node, $dom, $attributes);
		
		$node->appendChild($attribute = $dom->createElement('uhrzeit'));
		$attribute->appendChild($dom->createTextNode($value));
        $attribute->setAttribute('timestamp', $this->timestamp);
	}
}