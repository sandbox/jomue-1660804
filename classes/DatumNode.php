<?php
class WB_Parse_DatumNode extends WB_Parse_Node {
	protected $_xmlElement = 'tag';

	public function __construct($lineno, $name, $tag) {
		parent::__construct($lineno, $name);
		
		if(! is_int($tag)){
			$tag = mktime(null, null, null, $tag[1], $tag[2], $tag[0]);
		}
		$this->attributes['timestamp'] = $tag;
	}
	
	public function getTag() {
		return date('m-d-Y', $this->timestamp);
	}
	
	public function printNode() {
		$info = "";
		/*if(strlen($this->info)){
			$info = "\n<em>" . $this->info ."</em>";
		}*/
		return sprintf('%s &ndash; %s%s', date('l, d. m. Y', $this->timestamp), $this->name, $info);
	}
	
	protected function _getData($purpose = null){
		if($purpose == 'edit') return array();
		
		$data = $this->attributes;
		unset($data['timestamp']);
		$data['tag'] = date('l, d. m. Y', $this->timestamp);
		return $data;
	}
	
	protected function addXmlData($node, $dom){
		$attributes = $this->attributes;
		unset($attributes['timestamp']);
		$this->_addXmlAttributes($node, $dom, $attributes);
		
        $node->setAttribute('datum', date('Y-m-d', $this->timestamp));
	}
}